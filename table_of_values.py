import numpy as np

prime = lambda p: all(p % i != 0 for i in range(2, p))
perfect_number = lambda n: sum(i for i in range(1, n) if n % i == 0) == n

limit = 500

print(f'{"Num":3}:{"Prime number":>20}{"Perfect number":>20}\n{"-"*45}')
for i in range(2,limit):
    primeness = ""
    perfectness = ""
    if prime(i) or perfect_number(i):
        if prime(i): primeness = "Is prime"
        elif perfect_number(i): perfectness = "Is perfect"
        print(f"{i:3}:{primeness:>20}{perfectness:>20}")
print("-"*45)